from flask import Flask, request, render_template, jsonify, session, redirect, url_for
from flask_socketio import SocketIO, emit

from crud import execute, fetchone, fetchall
from functools import wraps
from flask_qrcode import QRcode

app = Flask(__name__)
qrcode = QRcode(app)
socketio = SocketIO(app)

app.secret_key = "PANGITDAWSIBOBBY?"


@app.after_request
def after_request(response):
    response.headers.add('Cache-Control', 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0')
    return response


def required_login(Function):
    @wraps(Function)
    def wrapper(*args, **kwargs):
        if not session.get('logs'):
            # flash("You need to log in first !")
            return redirect(url_for("admin_login"))
        else:
            return Function(*args, **kwargs)

    return wrapper


@app.route('/', methods=['GET', 'POST'])
def read_book_qr():
    if request.method == "POST":
        query = "SELECT isbn FROM books WHERE isbn = %s"
        data = fetchone(query, [request.form.get('qrcode')])
        if data:
            process_book(request.form.get('qrcode'))
            return jsonify({'error': False, 'data': 'Processing'})
        else:
            return jsonify({'error': True})
    return render_template("index.html")


def process_book(data):
    emit('process_book', data, broadcast=True, namespace='/process')


@app.route('/admin', methods=['GET', 'POST'])
def admin_login():
    if request.method == "POST":
        uname = request.form.get('username')
        pword = request.form.get('password')
        query = 'SELECT * FROM users WHERE username = %s and password = %s'
        data = fetchone(query, [uname, pword])
        if data:
            session['logs'] = True
            session['lname'] = data[4]
            session['fname'] = data[3]
            session['role'] = data[8]
            session['id'] = data[0]
            return jsonify({'data': 'success'})
        else:
            return jsonify({'error': '*Invalid username and password!'})
    return render_template("/admin/login.html")


@app.route('/users')
@required_login
def user_details():
    roles = "SELECT * FROM role"
    courses = "SELECT * FROM courses"
    users = "SELECT * FROM view_users"
    return render_template("/admin/user.html", role=fetchall(roles), users=fetchall(users), courses=fetchall(courses))


@app.route('/insert_data', methods=['POST'])
def users_insert():
    if request.method == "POST":
        if request.form['update-id'] == "":
            execute("INSERT INTO users VALUES(Null,%s,%s,%s,%s,%s,Null,NOW(),%s)",
                    [request.form['uname'], request.form['pass'],
                     request.form['fname'], request.form['lname'],
                     request.form['mname'], request.form['role']])

            execute("INSERT INTO student_info VALUES(Null,last_insert_id(),Null,%s,%s,%s)",
                    [request.form['idnumber'], request.form['course'], request.form['yearlvl']])
        else:
            execute(
                "UPDATE users SET username = %s, password = %s, firstname = %s , lastname = %s , middlename = %s , "
                "role_id = %s WHERE id = %s",
                [request.form['uname'],
                 request.form['pass'],
                 request.form['fname'],
                 request.form['lname'],
                 request.form['mname'],
                 request.form['role'],
                 request.form['update-id']
                 ])

            execute("UPDATE student_info SET id_number = %s, course_id = %s, year_level = %s WHERE user_id = %s",
                    [request.form['idnumber'],
                     request.form['course'],
                     request.form['yearlvl'],
                     request.form['update-id']
                     ])

        print('hi')
        return redirect(url_for('user_details'))


@app.route('/roles')
@required_login
def roles():
    roles = "SELECT * FROM role"
    return render_template("/admin/role.html", role=fetchall(roles))


@app.route('/insert_role', methods=['POST'])
def insert_role():
    if request.method == "POST":
        if request.form['role-id'] == "":
            execute("INSERT INTO role VALUES(Null,%s)",
                    [request.form['role']])
        else:
            execute("UPDATE role SET name = %s WHERE id = %s",
                    [request.form['role'],
                     request.form['role-id']
                     ])

        return redirect(url_for('role'))


@app.route('/logout')
def logout():
    session['id'] = session['fname'] = None
    session['logs'] = False
    return redirect(url_for('admin_login'))


@app.route('/books')
@required_login
def book_details():
    books = "SELECT b.id, isbn, title, description, img, qty, date_format(date_added, '%Y-%m-%d'), name, b.author_id " \
            "as auth, publisher, publish_date, publisher_address, topic, subject, table_of_contents FROM books b, " \
            "authors a where a.id = b.author_id "
    authors = "SELECT * FROM authors"
    return render_template("/admin/books.html", books=fetchall(books), authors=fetchall(authors))


@app.route('/insert_book', methods=['POST'])
def insert_book():
    if request.method == "POST":
        if request.form['update-id'] == "":
            execute("INSERT INTO books VALUES(Null,%s,%s,%s,%s,0,%s,now(),%s,%s,%s,%s,%s)", [
                        request.form['isbn'],
                        request.form['title'],
                        request.form['description'],
                        request.form['img'],
                        request.form['author'],
                        request.form['publisher'],
                        request.form['publish_date'],
                        request.form['publisher_address'],
                        request.form['topic'],
                        request.form['subject'],
                        request.form['table_of_contents'],
                    ])
        else:
            execute("UPDATE books SET isbn = %s, title = %s, description = %s, img = %s, author_id = %s, publisher = "
                    "%s, publish_date = %s, publisher_address = %s, topic = %s, subject = %s, table_of_contents = %s WHERE id = %s", [
                        request.form['isbn'],
                        request.form['title'],
                        request.form['description'],
                        request.form['img'],
                        request.form['author'],
                        request.form['publisher'],
                        request.form['publish_date'],
                        request.form['publisher_address'],
                        request.form['topic'],
                        request.form['subject'],
                        request.form['table_of_contents'],
                        request.form['update-id']
                    ])

        return redirect(url_for('book_details'))


@app.route('/authors')
@required_login
def authors():
    authors = "SELECT * FROM authors"
    return render_template("/admin/authors.html", authors=fetchall(authors))


@app.route('/insert_author', methods=['POST'])
def insert_author():
    if request.method == "POST":
        if request.form['author-id'] == "":
            execute("INSERT INTO authors VALUES(Null,%s)",
                    [request.form['name']])
        else:
            execute("UPDATE authors SET name = %s WHERE id = %s",
                    [request.form['name'],
                     request.form['author-id']
                     ])

        return redirect(url_for('authors'))



@app.route('/courses')
@required_login
def courses():
    course = "SELECT * FROM courses"
    return render_template("/admin/courses.html", courses=fetchall(course))



@app.route('/insert_course', methods=['POST'])
def insert_course():
    if request.method == "POST":
        if request.form['course-id'] == "":
            execute("INSERT INTO courses VALUES(Null, %s, %s)",
                    [request.form['name'], request.form['shortname']])
        else:
            execute("UPDATE courses SET name = %s, shortname = %s WHERE id = %s",
                    [
                        request.form['name'],
                        request.form['shortname'],
                        request.form['course-id']
                    ])

        return redirect(url_for('courses'))


@app.route('/borrow')
def borrow():
    return render_template("/frontend/borrow.html")


socketio.run(app, host="0.0.0.0", debug=True)

