import MySQLdb

conn = MySQLdb.connect(host='localhost', user='root', password='@cRsKakX', db='elibrary')
cur = conn.cursor()


def execute(query, args):
    try:
        cur.execute(query, args)
        conn.commit()
    except Exception as e:
        print(e)


def fetchone(query, args):
    try:
        cur.execute(query, args)
        data = cur.fetchone()
        return data
    except Exception as e:
        print(e)


def fetchall(query):
    try:
        cur.execute(query)
        data = cur.fetchall()
        return data
    except Exception as e:
        print(e)


def fetchmany(query, args):
    try:
        cur.execute(query, args)
        data = cur.fetchall()
        return data
    except Exception as e:
        print(e)
