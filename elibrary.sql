/*
 Navicat Premium Data Transfer

 Source Server         : Local
 Source Server Type    : MySQL
 Source Server Version : 50724
 Source Host           : localhost:3306
 Source Schema         : elibrary

 Target Server Type    : MySQL
 Target Server Version : 50724
 File Encoding         : 65001

 Date: 11/05/2020 14:20:52
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for authors
-- ----------------------------
DROP TABLE IF EXISTS `authors`;
CREATE TABLE `authors`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of authors
-- ----------------------------
INSERT INTO `authors` VALUES (1, 'Phillipe Monares');
INSERT INTO `authors` VALUES (2, 'Jen-Eric Domingo');
INSERT INTO `authors` VALUES (3, 'Florussel Gallibot');
INSERT INTO `authors` VALUES (4, 'Vergel Rey Abucayon');

-- ----------------------------
-- Table structure for books
-- ----------------------------
DROP TABLE IF EXISTS `books`;
CREATE TABLE `books`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `isbn` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `title` varchar(1024) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `description` varchar(2048) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `img` varchar(2048) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `qty` int(11) NULL DEFAULT NULL,
  `author_id` int(11) NULL DEFAULT NULL,
  `date_added` datetime(0) NULL DEFAULT NULL,
  `publisher` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `publish_date` datetime(0) NULL DEFAULT NULL,
  `publisher_address` varchar(512) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `topic` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `subject` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `table_of_contents` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `isbn`(`isbn`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of books
-- ----------------------------
INSERT INTO `books` VALUES (1, '9780986237720', 'Broom of Anger', 'Brunette explores the galvanizing quality of anger in this powerful debut collection.', 'http://books.google.com/books/content?id=7OCzDAEACAAJ&printsec=frontcover&img=1&zoom=1&source=gbs_api', 5, 2, '2020-05-06 14:37:23', 'E', '2020-05-10 00:00:00', 'F', 'G', 'H', '<p>tabless</p>');
INSERT INTO `books` VALUES (2, '9780986237721', 'Broom of Anger', 'Brunette explores the galvanizing quality of anger in this powerful debut collection.', 'http://books.google.com/books/content?id=7OCzDAEACAAJ&printsec=frontcover&img=1&zoom=1&source=gbs_api', 0, 2, '2020-05-10 13:29:07', 'I', '2020-05-12 00:00:00', 'J', 'K', 'L', '<p>contentss</p>');

-- ----------------------------
-- Table structure for courses
-- ----------------------------
DROP TABLE IF EXISTS `courses`;
CREATE TABLE `courses`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(512) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `shortname` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of courses
-- ----------------------------
INSERT INTO `courses` VALUES (1, 'Bachelor of Science in Information Technology', 'BSIT');
INSERT INTO `courses` VALUES (2, 'Bachelor of Science in Computer Science', 'BSCS');
INSERT INTO `courses` VALUES (3, 'Bachelor of Science in Accounting', 'BSA');

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES (1, 'admin');
INSERT INTO `role` VALUES (2, 'student');
INSERT INTO `role` VALUES (3, 'principal');
INSERT INTO `role` VALUES (4, 'badlungon');

-- ----------------------------
-- Table structure for student_info
-- ----------------------------
DROP TABLE IF EXISTS `student_info`;
CREATE TABLE `student_info`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NULL DEFAULT NULL,
  `qr_code` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_number` varchar(25) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `course_id` int(11) NULL DEFAULT NULL,
  `year_level` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of student_info
-- ----------------------------
INSERT INTO `student_info` VALUES (1, 1, NULL, '16-11661', 2, '20202020');
INSERT INTO `student_info` VALUES (2, 4, NULL, '16-10390', 1, '4th');
INSERT INTO `student_info` VALUES (3, 5, NULL, '21312', 2, '2000');
INSERT INTO `student_info` VALUES (4, 9, NULL, '123123', 1, '123123');
INSERT INTO `student_info` VALUES (5, 10, NULL, '211312', 1, '123');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `password` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `firstname` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `lastname` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `middlename` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `avatar` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `date_joined` datetime(0) NULL DEFAULT NULL,
  `role_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'admin', 'admin', 'Kevin', 'Fajardo', 'P', NULL, NULL, 1);
INSERT INTO `users` VALUES (4, 'jebdomingo', 'M4h47k0e!122', 'Jen-Eric', 'Domingo', 'Bacani', NULL, '2020-05-08 16:55:24', 3);
INSERT INTO `users` VALUES (5, 'hi', 'hello', 'hi', 'hi', 'hello', NULL, '2020-05-08 18:35:57', 2);
INSERT INTO `users` VALUES (9, '123123', '123123', '123123', '123123', '123123', NULL, '2020-05-10 10:37:25', 2);
INSERT INTO `users` VALUES (10, 'admin', 'admin', '12331', '1123', '123', NULL, '2020-05-10 14:38:55', 2);

-- ----------------------------
-- View structure for view_users
-- ----------------------------
DROP VIEW IF EXISTS `view_users`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `view_users` AS SELECT
	users.id,
	firstname,
	lastname,
	middlename,
	username,
	role.`name` as role,
	student_info.course_id,
	student_info.year_level,
	student_info.id_number,
	users.`password`,
	users.role_id,
	courses.`name`
FROM
	users,
	courses,
	student_info,
	role 
WHERE
	student_info.user_id = users.id 
	AND users.role_id = role.id
	AND courses.id = student_info.course_id ;

SET FOREIGN_KEY_CHECKS = 1;
