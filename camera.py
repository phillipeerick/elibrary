from time import sleep

import cv2
import requests
import pyzbar.pyzbar as pyzbar
from pyzbar.pyzbar import ZBarSymbol
from pytesseract import pytesseract, Output

cap = cv2.VideoCapture(0)
font = cv2.FONT_HERSHEY_PLAIN

pytesseract.tesseract_cmd = "C:\\Program Files\\Tesseract-OCR\\tesseract.exe"
store = ['']

book = False
url = 'http://127.0.0.1:5000/'

while True:
    _, frame = cap.read()

    # decodedTesseract = pytesseract.image_to_string(frame)
    decodedQR = pyzbar.decode(frame, symbols=[ZBarSymbol.QRCODE])
    for obj in decodedQR:
        if store[0] != str(obj.data.decode('utf-8')):
            store[0] = str(obj.data.decode('utf-8'))
            print(store[0])
            # response = requests.post(url, data={'qrcode': store[0]})
            # jsonObject = response.json()
            # if jsonObject['error']:
            #     print(jsonObject['error'])
            # else:
            #     print("ISBN existed!")

    cv2.imshow("Scan QR Code", frame)
    key = cv2.waitKey(1)


    # Process Object Detection
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    gray = cv2.GaussianBlur(gray, (3, 3), 0)
    edged = cv2.Canny(gray, 10, 250)
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (7, 7))
    closed = cv2.morphologyEx(edged, cv2.MORPH_CLOSE, kernel)
    (cnts, _) = cv2.findContours(closed.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    total = 0

    for c in cnts:
        peri = cv2.arcLength(c, True)
        approx = cv2.approxPolyDP(c, 0.02 * peri, True)

        if len(approx) == 4:
            cv2.drawContours(frame, [approx], -1, (0, 255, 0), 4)
            total += 1
            book = True

    if book:
        print(total)
        book = False

    if key == 27:
        break


    # img = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    #
    # thresh = cv2.threshold(img, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]
    # result = 255 - thresh
    # text = pytesseract.image_to_data(result, lang='eng', config='--psm 6', output_type=Output.DICT)
    # data = []
    # for row in text['text']:
    #     if row != '':
    #         print(row)
    #         data.append(row)
    #
    # requests.post(url, data={'qrcode': " ".join(data)})
    # cv2.imshow('Capture', result)

